import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LoaderComponent } from './shared/components/loader/loader.component';
import { LoginComponent } from './pages/login/login.component';
import { AppRoutingModule } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';


// Import ReactiveFormsModule
import { ReactiveFormsModule } from '@angular/forms';

//SERVICIOS
import { usuarioService } from './shared/services/usuario.service';

//Redux
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { reducers } from './redux/config.reducers';
import { AdminComponent } from './pages/admin/admin.component';
import { AdminModule } from './pages/admin/admin.module';
import { Authguard } from './guards/authguards';
import { FacturacionComponent } from './pages/admin/facturacion/facturacion.component';


@NgModule({
  declarations: [
    // LoaderComponent,
    LoginComponent,
    AppComponent,
    FacturacionComponent,
    // AdminComponent
  ],
  imports: [
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument(),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    AdminModule
  ],
  providers: [
    usuarioService,
    Authguard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
