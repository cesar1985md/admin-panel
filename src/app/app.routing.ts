import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { LoginComponent } from "./pages/login/login.component";
import { HomeComponent } from "./pages/admin/home/home.component";
import { AdminComponent } from "./pages/admin/admin.component";
import { Authguard } from "./guards/authguards";


const routes: Routes = [
    {
        path: "",
        component: AdminComponent,
        canActivate: [Authguard],
    },
    {
        path: "login",
        component: LoginComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {

}