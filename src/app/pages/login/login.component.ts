import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

// REDUX
import { Store } from '@ngrx/store';
import { ACTIONS_USUARIO_TYPES } from '../../redux/actions/usuario.actions';
import { AppState } from '../../redux/config.reducers';
import { UsuarioState } from '../../redux/states/usuario.state';
import { Usuario } from '../../shared/models/usuario.model';
import { usuarioService } from '../../shared/services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public alertmsn: string = "";

  public usuario: Usuario = new Usuario(null, "", "", "", false);

  constructor(
    private _usuarioService: usuarioService,
    private _store: Store<AppState>,
    private _router: Router
  ) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      'nombre': new FormControl(),
      'pass': new FormControl()
    });
  }

  acceder() {
    this._usuarioService.loginBBDD(this.loginForm.value.nombre, this.loginForm.value.pass).subscribe((res:any) => {
      if(res.result === "OK"){
        let usuario: Usuario = res.data;
        usuario.isLoged = true;
        this._store.dispatch({ type: ACTIONS_USUARIO_TYPES.LOGIN, payload: usuario });
        this._router.navigate(["/admin/home"]);
      }else{
        alert(res.mensaje);
      }
    });

  }
}
