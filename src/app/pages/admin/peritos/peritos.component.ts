import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Validators } from "@angular/forms";
import { Perito } from '../../../shared/models/peritos.model';
import { InputForm } from '../../../shared/models/generic.model';
import { peritosService } from '../../../shared/services/peritos.service';
import { Respuesta } from '../../../shared/models/usuario.model';




@Component({
  selector: 'app-peritos',
  templateUrl: './peritos.component.html',
  styleUrls: ['./peritos.component.css']
})
export class PeritosComponent implements OnInit {
  public peritos: Array<Perito>;
  public peritos$: Observable<Array<Perito>>
  public mensaje: string;

  public titulo: String;
  public columnas: Array<any>;
  public respuesta: Respuesta<Array<Perito>>;


  public inputsForm: Array<InputForm>;

  constructor(
    private _peritos: peritosService
  ) { }

  ngOnInit() {

    this.peritos$ = this._peritos.getPeritos();
    this.titulo = " TABLA PERITOS";
    this.columnas = [{ Idperito: "Id" }, { Perito: "Perito" }];

    this.inputsForm = [
      new InputForm("text", "Nombre", "nombre", [Validators.required, Validators.maxLength(25)]),
    ];
  }

  datosForm($event): void {
    let perito: Perito = new Perito(null, $event.nombre);
    this._peritos.addPerito(perito).subscribe((res: Respuesta<any>) => {
      if (res.code == 200) {
      }
    });
  }
}
