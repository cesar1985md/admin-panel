import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { HomeComponent } from "./home/home.component";
import { PeritosComponent } from "./peritos/peritos.component";
import { CorredoresComponent } from "./corredores/corredores.component";
import { CompaniasComponent } from "./companias/companias.component";
import { PeritacionesComponent } from "./peritaciones/peritaciones.component";
import { AdminComponent } from "./admin.component";
import { Authguard } from "../../guards/authguards";
import { ResponsablesComponent } from "./responsables/responsables.component";
import { FacturacionComponent } from "./facturacion/facturacion.component";

const ROUTES: Routes = [{
    path: 'admin',
    component: AdminComponent,
    canActivate: [Authguard],
    children: [
        {
            path: 'home',
            component: HomeComponent,
            data: {icon: 'home', ruta:'Inicio'},
            //canActivate: [ AuthGuard ] 
        },{
            path: 'peritos',
            component: PeritosComponent,
            data: {icon: 'accessibility', ruta:'Peritos'},
            //canActivate: [ AuthGuard ]
        },{
            path: 'corredores',
            component: CorredoresComponent,
            data: {icon: 'accessibility', ruta:'Corredores'},
            //canActivate: [ AuthGuard ]
        },{
            path: 'companias',
            component: CompaniasComponent,
            data: {icon: 'card_travel', ruta:'Compañías'},
            //canActivate: [ AuthGuard ]
        },{
            path: 'peritacones',
            component: PeritacionesComponent,
            data: {icon: 'assignment', ruta:'Peritaciones'},
            //canActivate: [ AuthGuard ]
        },{
            path: 'responsables',
            component: ResponsablesComponent,
            data: {icon: 'assignment', ruta:'Responsables'},
            //canActivate: [ AuthGuard ]
        },{
            path: 'facturacion',
            component: FacturacionComponent,
            data: {icon: 'assignment', ruta:'Facturacion'},
            //canActivate: [ AuthGuard ]
        }
    ]
}];

@NgModule({
    imports: [RouterModule.forChild(ROUTES)],
    exports: [RouterModule]
})

export class AdminRoutingModule {

}