import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Validators } from "@angular/forms";
import { Corredor } from '../../../shared/models/corredor.model';
import { InputForm } from '../../../shared/models/generic.model';
import { corredoresService } from '../../../shared/services/corredores.service';

@Component({
  selector: 'app-corredores',
  templateUrl: './corredores.component.html',
  styleUrls: ['./corredores.component.css']
})
export class CorredoresComponent implements OnInit {
  public corredores: Array<Corredor>;
  public corredores$: Observable<Array<Corredor>>
  public titulo:String;
  public columnas:Array<any>;

  public inputsForm: Array<InputForm>;

  constructor(
    private _corredores: corredoresService
  ) { }

  ngOnInit() {

    this.corredores$ = this._corredores.getCorredores();
    this.titulo =  " TABLA CORREDORES";
    this.columnas = [{IdCorredor:"Id"},{Corredor:"Corredor"}];

    this.inputsForm = [
      new InputForm("text", "Nombre", "nombre",[Validators.required,Validators.maxLength(25)])
    ];
  }

  datosForm ($event):void {
    
    console.log("ESTE SE GUARDA EN SERVICIO",$event);
  }

}
