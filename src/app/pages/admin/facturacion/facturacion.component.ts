import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.component.html',
  styleUrls: ['./facturacion.component.css']
})
export class FacturacionComponent implements OnInit {
  myGroup: FormGroup;
  constructor() { }

  ngOnInit() {
    
    this.myGroup = new FormGroup({
      firstName: new FormControl()
   });    
  }

}
