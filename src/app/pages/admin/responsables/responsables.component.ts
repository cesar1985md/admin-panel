import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Validators } from "@angular/forms";
import { InputForm } from '../../../shared/models/generic.model';
import { responsablesService } from '../../../shared/services/responsables.service';
import { Responsable } from '../../../shared/models/responsable.model';

@Component({
  selector: 'app-responsables',
  templateUrl: './responsables.component.html',
  styleUrls: ['./responsables.component.css']
})

export class ResponsablesComponent implements OnInit {
  public responsables: Array<Responsable>;
  public responsables$: Observable<Array<Responsable>>
  public titulo:String;
  public columnas:Array<any>;
  public inputsForm: Array<InputForm>;

  constructor(
    private _responsables: responsablesService
  ) { }

  ngOnInit() {
    this.responsables$ = this._responsables.getResponsables();
    this.titulo =  " TABLA RESPONSABLES";
    this.columnas = [{IdResp:"Id"},{Resp:"Nombre"}];

    this.inputsForm = [
      new InputForm("text", "Nombre",[Validators.required,Validators.maxLength(25)])
    ];
  }

  datosForm ($event):void {
    console.log("ESTE SE GUARDA EN SERVICIO",$event);
  }
}
