import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Import ReactiveFormsModule
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from '../../app.component';
import { LoaderComponent } from '../../shared/components/loader/loader.component';
import { TopbarComponent } from '../../shared/components/topbar/topbar.component';
import { LeftsidebarComponent } from '../../shared/components/leftsidebar/leftsidebar.component';
import { RightsidebarComponent } from '../../shared/components/rightsidebar/rightsidebar.component';
import { HomeComponent } from './home/home.component';
import { PeritosComponent } from './peritos/peritos.component';
import { BasictableComponent } from '../../shared/components/basictable/basictable.component';
import { PeritacionesComponent } from './peritaciones/peritaciones.component';
import { GenericformComponent } from '../../shared/components/genericform/genericform.component';
import { LoginComponent } from '../login/login.component';
import { CompaniasComponent } from './companias/companias.component';
import { CorredoresComponent } from './corredores/corredores.component';
import { ResponsablesComponent } from './responsables/responsables.component';



//SERVICIOS
import { peritosService } from '../../shared/services/peritos.service';
import { peritacionesService } from '../../shared/services/peritaciones.service';
import { companiasService } from '../../shared/services/companias.service';
import { usuarioService } from '../../shared/services/usuario.service';
import { corredoresService } from '../../shared/services/corredores.service';

//Redux
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { reducers } from '../../redux/config.reducers';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from '../../app.routing';
import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routes.module';
import { responsablesService } from '../../shared/services/responsables.service';


@NgModule({
  declarations: [
    // AppComponent,
    LoaderComponent,
    TopbarComponent,
    LeftsidebarComponent,
    RightsidebarComponent,
    HomeComponent,
    PeritosComponent,
    BasictableComponent,
    PeritacionesComponent,
    GenericformComponent,
    // LoginComponent,
    CompaniasComponent,
    CorredoresComponent,
    AdminComponent,
    ResponsablesComponent
  ],
  imports: [
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument(),
    BrowserModule,
    // AppRoutingModule,
    AdminRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    // AdminModule,
  ],
  exports: [
    // TopbarComponent,
    AdminComponent
  ],
  providers: [
    peritosService,
    peritacionesService,
    companiasService,
    usuarioService,
    corredoresService,
    responsablesService
  ],
  bootstrap: [AdminComponent]
})
export class AdminModule { }
