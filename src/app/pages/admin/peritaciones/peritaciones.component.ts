import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Peritaciones } from '../../../shared/models/peritaciones.model';
import { peritacionesService } from '../../../shared/services/peritaciones.service';


@Component({
  selector: 'app-peritaciones',
  templateUrl: './peritaciones.component.html',
  styleUrls: ['./peritaciones.component.css']
})
export class PeritacionesComponent implements OnInit {
  public peritaciones: Array<Peritaciones>;
  public peritaciones$: Observable<Array<Peritaciones>>
  public titulo:String;
  public columnas:Array<any>;

  constructor(
    private _peritaciones: peritacionesService
  ) { }

  ngOnInit() {

    this.peritaciones$ = this._peritaciones.getPeritaciones();
    
    this.titulo =  " TABLA PERITACIONES";

    this.columnas = [
      {IdPeritacion:"Id"},
      {NRef:"Referencia"},
      {RefAsitur:"Ref Asitur"},
      {Perito:"Perito"},
      {Corredor:"Corredor"},
      {Poliza:"Poliza"},
      {NSiniestro:"Siniestro"},
      {TipoVia:"Tipo Vía"},
      {Av:"Av"},
      {Direccion:"Dirección"}];
  }
}
