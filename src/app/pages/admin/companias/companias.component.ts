import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Validators } from "@angular/forms";
import { Compania } from '../../../shared/models/compania.model';
import { InputForm } from '../../../shared/models/generic.model';
import { companiasService } from '../../../shared/services/companias.service';

@Component({
  selector: 'app-companias',
  templateUrl: './companias.component.html',
  styleUrls: ['./companias.component.css']
})

export class CompaniasComponent implements OnInit {
  public companias: Array<Compania>;
  public companias$: Observable<Array<Compania>>
  public titulo:String;
  public columnas:Array<any>;
  public inputsForm: Array<InputForm>;

  constructor(
    private _companias: companiasService
  ) { }

  ngOnInit() {
    this.companias$ = this._companias.getCompanias();
    this.titulo =  " TABLA COMPAÑÍAS";
    this.columnas = [{Idcompania:"Id"},{Compania:"Nombre Compañía"},{Kilometraje:"Kilometraje"},{Fotos:"Fotos"}];
    this.inputsForm = [
      new InputForm("text", "Nombre Compañia", "nombre",[Validators.required,Validators.maxLength(25)]),
      new InputForm("text", "Nombre", "apellidos",[Validators.required,Validators.maxLength(25)])
    ];
  }

  datosForm ($event):void {
    console.log("ESTE SE GUARDA EN SERVICIO",$event);
  }
}
