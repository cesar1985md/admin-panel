import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Router } from "@angular/router";
import { usuarioService } from "../shared/services/usuario.service";
import { AppState } from "../redux/config.reducers";
import { Store } from "@ngrx/store";
import { UsuarioState } from "../redux/states/usuario.state";
import { Observable } from "rxjs/Observable";



@Injectable()
export class Authguard implements CanActivate {
    constructor(
        private store: Store<AppState>,
        private router: Router
    ) {}

    canActivate():Observable<boolean> {

        return this.store.select("usuario").map((userState: UsuarioState) => {
            // return userState.usuario.isLoged;
            console.log(userState.usuario);
            if(userState.usuario.isLoged){
                return true;
            }else{
                this.router.navigate(["login"]);
                return false;
            }
        });
    }
}