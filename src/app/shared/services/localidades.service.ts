import { Http, Response, Headers } from '@angular/http';
import { HttpClient, HttpParams, HttpResponse } from "@angular/common/http";
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class localidadesService {
    constructor(private _http: HttpClient){

    }

    public getLocalidades() {
        return this._http.get(environment.urlApi + 'localidades/getAll', {observe: "response"})
        .map((res : HttpResponse<any>) => res.body).map(res => res.data)
    }

    public addLocalidad() {
        
    }
  }