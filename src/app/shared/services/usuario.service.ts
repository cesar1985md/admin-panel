import { Http, Response, Headers } from '@angular/http';
import { HttpClient, HttpParams, HttpResponse } from "@angular/common/http";
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
// REDUX
import { Store } from '@ngrx/store';
import { ACTIONS_USUARIO_TYPES } from '../../redux/actions/usuario.actions';
import { AppState } from '../../redux/config.reducers';
import { UsuarioState } from '../../redux/states/usuario.state';
import { Usuario } from '../../shared/models/usuario.model';
import { Subject } from 'rxjs/Subject';



@Injectable()
export class usuarioService {

    public usuario: Usuario = new Usuario(null, "", "", "", false);
    public returnUrl: string;

    constructor(
        private _http: HttpClient,
        private _store: Store<AppState>
    ) {
    }

    // public getPeritos() {
    //     return this._http.get('http://localhost:8080/peritos_back/index.php/peritos', {observe: "response"})
    //     .map((res : HttpResponse<any>) => res.body).map(res => res.data)
    // }
    loginBBDD(usuario, pass): Observable<any> {
        let subject = new Subject<any>();
        setTimeout(() => {
            subject.next({
                result: "OK",
                mensaje: "Error login",
                data: new Usuario(1, "Yo", "1234", "1234", true)
            });
        },1000);
        return subject.asObservable();
    }
}