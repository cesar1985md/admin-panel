import { Http, Response, Headers } from '@angular/http';
import { HttpClient, HttpParams, HttpResponse, HttpHeaders } from "@angular/common/http";
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import { Perito } from '../models/peritos.model';
import { environment } from '../../../environments/environment';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { Respuesta } from '../models/usuario.model';
import { Observable } from 'rxjs/Observable';

let headers = new HttpHeaders();
headers = headers.append("content-type", "text/plain");
headers.append('Accept', 'application/json');
headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
headers.append('Access-Control-Allow-Headers', "X-Requested-With, Content-Type, Origin");

@Injectable()
export class peritosService {
    constructor(private _http: HttpClient){

    }

    public getPeritos(): Observable<Array<Perito>> {
        return this._http.get(environment.urlApi + 'peritos/getAll', {observe: "response"})
        .map((res : HttpResponse<any>) => res.body).map((res: Respuesta<Array<Perito>>) => {
            return res.data;
        });
    }

    public addPerito(perito:Perito):Observable<Respuesta<any>> {
        return this._http.post(environment.urlApi + 'peritos/update', perito,  {observe: "response", headers: headers})
               .map((res: HttpResponse<any>) => res.body);
    }
  }