import { Http, Response, Headers } from '@angular/http';
import { HttpClient, HttpParams, HttpResponse, HttpHeaders } from "@angular/common/http";
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { Respuesta } from '../models/usuario.model';
import { Observable } from 'rxjs/Observable';
import { Responsable } from '../models/responsable.model';

let headers = new HttpHeaders();
headers = headers.append("content-type", "text/plain");
headers.append('Accept', 'application/json');
headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
headers.append('Access-Control-Allow-Headers', "X-Requested-With, Content-Type, Origin");

@Injectable()
export class responsablesService {
    constructor(private _http: HttpClient){

    }

    public getResponsables(): Observable<Array<Responsable>> {
        return this._http.get(environment.urlApi + 'responsables/getAll', {observe: "response"})
        .map((res : HttpResponse<any>) => res.body).map((res: Respuesta<Array<Responsable>>) => {
            return res.data;
        });
    }

    public addResposable(responsable:Responsable):Observable<Respuesta<any>> {
        return this._http.post(environment.urlApi + 'responsables/update', responsable,  {observe: "response", headers: headers})
               .map((res: HttpResponse<any>) => res.body);
    }
  }