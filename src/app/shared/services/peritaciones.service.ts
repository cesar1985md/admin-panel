import { Http, Response, Headers } from '@angular/http';
import { HttpClient, HttpParams, HttpResponse } from "@angular/common/http";
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class peritacionesService {
    constructor(private _http: HttpClient){

    }

    public getPeritaciones() {
        return this._http.get(environment.urlApi + 'peritaciones/getAll', {observe: "response"})
        .map((res : HttpResponse<any>) => res.body).map(res => res.data)
    }
  }