export class InputForm {
    public type: string;
    public label: string;
    public nombre: string;
    public validadores: Array<any>;
    constructor(type, label, nombre, validadores?) {
        this.type = type;
        this.label = label;
        this.nombre = nombre;
        this.validadores = validadores;
    }
}