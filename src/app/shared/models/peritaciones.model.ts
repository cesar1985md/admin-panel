export class Peritaciones {
    public IdPeritacion: number;
    public NRef: string;
    public RefAsitur: string;
    public Perito: number;
    public Compania: number;
    public Corredor: number;
    public Poliza: string;
    public NSiniestro: string;
    public TipoVia: string;
    public Av: string;
    public Direccion: string;

//     public IdPeritacion: string;
// public NRef: string;
// public RefAsitur: string;
// public Perito: string;
// public Compania: string;
// public Corredor: string;
// public Poliza: string;
// public NSiniestro: string;
// public TipoVia: string;
// public Av: string;
// public Direccion: string;
// public Poblacion: string;
// public Municipio: string;
// public CP: string;
// public FechaEncargo: string;
// public FechaFactura: string;
// public NFactura: string;
// public Pago: string;
// public Honorarios: string;
// public Kilometros: string;
// kmEu
// public HonorariosT: string;
// public IVA: string;
// public IRPF: string;
// public Apercibir: string;
// public Ampliaciones: string;
// public Fraude: string;
// public Inspecciones: string;
// public Juicio: string;
// public Descripcion: string;
// public TareaPend: string;
// public Fotos: string;
// public FotosEu: string;
// public Varios: string;
// public HonorariosPo: string;
// public HonorariosPe: string;
// public FotosPo: string;
// public FotosPe: string;
// kmPo
// kmPe
// public VariosPo: string;
// public VariosPe: string;
// public Peritado: string;
// public TipoSiniestro: string;
// public Telefono 1: string;
// public Telefono 2: string;
// public Telefono 3: string;
// public Telefono 4: string;
// public Reparador: string;
// public Asegurado: string;
// public Facturado: string;
// public FechaAvance: string;
// public Estado: string;
// public Noticias: string;
// public Notas: string;
// public Respons: string;
// public Efecto: string;
// public Franquicias: string;
// public Ocurrencia: string;
// public Portal: string;
// public Piso: string;
// public Letra: string;
// public NuevaTarea: string;
// public Provision: string;
// public Prov: string;
// public Devuelto: string;
// public ICR

    constructor(IdPeritacion, NRef, RefAsitur, Perito, Compania, Corredor, Poliza, NSiniestro, TipoVia, Av, Direccion) {
        this.IdPeritacion = IdPeritacion;
        this.NRef = NRef;
        this.RefAsitur = RefAsitur;
        this.Perito = Perito;
        this.Compania = Compania;
        this.Corredor = Corredor;
        this.Poliza = Poliza;
        this.NSiniestro = NSiniestro;
        this.TipoVia = TipoVia;
        this.Av = Av;
        this.Direccion = Direccion;
    }
}