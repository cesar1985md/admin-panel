export class Usuario {
    public id: number;
    public nombre: string;
    public pass: string;
    public token: string;
    public isLoged: boolean;
    constructor(id=null,nombre,pass,token,isLoged=false) {
        this.id = id;
        this.nombre = nombre;
        this.pass = pass;
        this.token = token;
        this.isLoged = isLoged;
    }
}

export class Respuesta<T> {
    public code: number;
    public data: T;
    public mensaje: string;
    public result: string;
    constructor(code:number, data:T,mensaje:string,result:string) {
        this.code = code;
        this.data = data;
        this.mensaje = mensaje;
        this.result = result;
    }
}