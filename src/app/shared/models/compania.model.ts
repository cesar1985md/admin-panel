export class Compania {
    public Idcompania: number;
    public Compania: string;
    public Kilometraje: number;
    public Fotos: number;
    constructor(Idcompania=null, Compania, Kilometraje, Fotos) {
        this.Idcompania = Idcompania;
        this.Compania = Compania;
        this.Kilometraje = Kilometraje;
        this.Fotos = Fotos;
    }
}