export class Perito {
    public idperito: number;
    public nombre: string;
    constructor(idperito, nombre) {
        this.idperito = idperito;
        this.nombre = nombre;
    }
}