export class Corredor {
    public id: number;
    public nombre: string;
    constructor(id, nombre) {
        this.id = id;
        this.nombre = nombre;
    }
}