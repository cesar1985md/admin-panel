import { Component, OnInit, AfterViewInit } from '@angular/core';
// ROUTER
import { Routes, RouterModule, Route, Router } from "@angular/router";


// REDUX
import { Store } from '@ngrx/store';
import { ACTIONS_USUARIO_TYPES } from '../../../redux/actions/usuario.actions';
import { AppState } from '../../../redux/config.reducers';
import { UsuarioState } from '../../../redux/states/usuario.state';
import { Usuario } from '../../models/usuario.model';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-leftsidebar',
  templateUrl: './leftsidebar.component.html',
  styleUrls: ['./leftsidebar.component.css']
})
export class LeftsidebarComponent implements OnInit, AfterViewInit {
  public leftmenu: Array<Route>;

  public usuario: Usuario = null;

  constructor(
    private rutas: Router,
    private _store: Store<AppState>
  ) { }

  ngOnInit() {
    console.log(this.rutas.config);
    this.leftmenu = this.rutas.config[2].children.filter(r => {
      if (r.path != "**" && r.path != "login") {
        return r;
      }
    });

    //LLAMADA A STORE
    this._store.select('usuario').subscribe((usuariostate) => {
      this.usuario = usuariostate.usuario;
    })
  }

  ngAfterViewInit() {
    this.isFirstTime();
  }
  
  isFirstTime (){
    if (typeof $.fn.slimScroll != 'undefined') {
      var configs = $.AdminBSB.options.leftSideBar;
      var height = ($(window).height() - ($('.legal').outerHeight() + $('.user-info').outerHeight() + $('.navbar').innerHeight()));
      var $el = $('.list');

      $el.slimscroll({
        height: height + "px",
        color: configs.scrollColor,
        size: configs.scrollWidth,
        alwaysVisible: configs.scrollAlwaysVisible,
        borderRadius: configs.scrollBorderRadius,
        railBorderRadius: configs.scrollRailBorderRadius
      });

      //Scroll active menu item when page load, if option set = true
      if ($.AdminBSB.options.leftSideBar.scrollActiveItemWhenPageLoad) {
        var activeItemOffsetTop = $('li.active')[0].offsetTop
        if (activeItemOffsetTop > 150) $el.slimscroll({ scrollTo: activeItemOffsetTop + 'px' });
      }
    }
  }

}