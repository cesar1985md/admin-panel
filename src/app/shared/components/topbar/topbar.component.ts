import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../redux/config.reducers';
import { Router } from '@angular/router';
import { Usuario } from '../../models/usuario.model';
import { ACTIONS_USUARIO_TYPES } from '../../../redux/actions/usuario.actions';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  public usuario: Usuario = new Usuario(0,"","","",false);
  constructor(
    private _store: Store<AppState>,
    private _router: Router
  ) { }

  ngOnInit() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  cerrarsesion() {
    // ELIMINAR TOKEN.
    // LLAMAR AL SERVICIO DE CERRAR SESION
    this.usuario.isLoged = false;
    this._store.dispatch({ type: ACTIONS_USUARIO_TYPES.LOGIN, payload: this.usuario });
    this._router.navigate(["/login"]);
  }
}
