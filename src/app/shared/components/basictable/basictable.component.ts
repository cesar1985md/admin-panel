import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';

declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-basictable',
  templateUrl: './basictable.component.html',
  styleUrls: ['./basictable.component.css']
})
export class BasictableComponent implements OnInit {

  //@Input() data: Observable<Array<Perito>>;
  @Input() listado: Observable<Array<any>>;
  @Input() tituloTabla: String;
  @Input() columnasTabla: Array<String>
  @Input() isExportable: boolean;

  public listadoRows: Array<any> = [];
  public columnasTitle: any;
  public columnasValues: any;

  public _idTable: number;
  
  constructor() { }

  ngOnInit() {
    this._idTable = this.random();
    this.listado.subscribe((res: Array<any>) => {
        this.listadoRows = res;
console.log(this.listadoRows);
        // Se pone un setTimeout para que le de tiempo a cargar las librerias de JQuery.
        setTimeout(() => {
          if(!this.isExportable){
            $('#dataTable_'+this._idTable).DataTable({
                responsive: true
            });
          }else{
            //Exportable table
            $('#dataTable_'+this._idTable).DataTable({
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
          }
      },200);
    });
    this.columnasValues = this.columnasTabla.map(c => Object.keys(c)[0]);
    this.columnasTitle = this.columnasTabla.map(c => c[Object.keys(c)[0]]);
  }

  random(){
    return Math.round(Math.random()*1000);
  }

}
