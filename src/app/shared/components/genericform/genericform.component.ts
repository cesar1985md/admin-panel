import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';

//Import the API for building a form
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InputForm } from '../../models/generic.model';


declare var jquery:any;
declare var $ :any;

@Component({
  selector: 'app-genericform',
  templateUrl: './genericform.component.html',
  styleUrls: ['./genericform.component.css']
})
export class GenericformComponent implements OnInit {
  genericForm: FormGroup;
  @Input() listadoInput: Array<InputForm>;
  @Input() mensajeInsertar: string;
  @Output() onSendForm: EventEmitter<FormGroup> = new EventEmitter();

  public inputValues: any;
  public inputLabel: any;
  public isSend: boolean;

  constructor() { }

  ngOnInit() {
      this.genericForm = new FormGroup({});
      this.isSend = false;
      this.listadoInput.forEach(element => {
          this.genericForm.addControl(element.nombre, new FormControl("",element.validadores));
      });
}

  onFormSubmit(){
    if (this.genericForm.valid) {
      this.isSend = true;
      this.onSendForm.emit(this.genericForm.value);
    } else {
      this.isSend = false;
    }
  }

  ngAfterViewInit() {
    /* Input - Function ========================================================================================================
    *  You can manage the inputs(also textareas) with name of class 'form-control'
    *  
    */
    this.activate();
  }

  activate() {
      //On focus event
      $('.form-control').focus(function () {
          $(this).parent().addClass('focused');
      });

      //On focusout event
      $('.form-control').focusout(function () {
          var $this = $(this);
          if ($this.parents('.form-group').hasClass('form-float')) {
              if ($this.val() == '') { $this.parents('.form-line').removeClass('focused'); }
          }
          else {
              $this.parents('.form-line').removeClass('focused');
          }
      });

      //On label click
      $('body').on('click', '.form-float .form-line .form-label', function () {
          $(this).parent().find('input').focus();
      });

      //Not blank form
      $('.form-control').each(function () {
          if ($(this).val() !== '') {
              $(this).parents('.form-line').addClass('focused');
          }
      });
  }
  
//==========================================================================================================================
}
