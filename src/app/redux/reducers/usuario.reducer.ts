import { UsuarioState } from "../states/usuario.state";
import { UsuarioAction, ACTIONS_USUARIO_TYPES } from "../actions/usuario.actions";
import { Usuario } from "../../shared/models/usuario.model";

export const usuarioReducer = (state: UsuarioState = UsuarioState.initialState(), action: UsuarioAction):UsuarioState => {
    switch (action.type) {
        case ACTIONS_USUARIO_TYPES.LOGIN:
            return {...state,usuario: action.payload};
        default: 
            return state; 
    }  
}