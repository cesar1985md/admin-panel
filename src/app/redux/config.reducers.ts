import { usuarioReducer } from "./reducers/usuario.reducer";
import { UsuarioState } from "./states/usuario.state";


// Se incluyen todos lo reducers
export const reducers = {
    usuario: usuarioReducer,
}

export interface AppState {
    usuario: UsuarioState
}
