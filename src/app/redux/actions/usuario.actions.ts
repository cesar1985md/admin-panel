import { Action } from '@ngrx/store';
import { Usuario } from '../../shared/models/usuario.model';


export const ACTIONS_USUARIO_TYPES = {
    LOGIN: 'loginUsuario'
}

export class UsuarioAction implements Action {
    type: string;
    payload?: Usuario;
}