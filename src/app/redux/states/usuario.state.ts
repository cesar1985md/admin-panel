import { Usuario } from "../../shared/models/usuario.model";

export class UsuarioState {
    public usuario: Usuario;

    static initialState() {
        return {usuario: new Usuario(0,"","","",false)};
    }
}